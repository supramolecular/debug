# Band structure of BaSnO3 from OpenMX and Vasp

1. vasp, LDA.

   lattice constant a=4.07 $\AA$, band gap $E_g=1.30$ eV.
   <img src="assets/image-20210611091425593.png" alt="image-20210611091425593" style="zoom:33%;" />          <img src="assets/image-20210611091636676.png" alt="image-20210611091636676" style="zoom:60%;" />  

   The lower panel in the figure above is the band from the reference https://journals.aps.org/prb/abstract/10.1103/PhysRevB.97.104310.

2. vasp, GGA

   lattice constant a =4.149 $\AA$. Band gap $E_g=0.65$ eV.

   <img src="assets/image-20210611092123395.png" alt="image-20210611092123395" style="zoom:33%;" /> 

3. openmx, LDA

   lattice constant a=4,15 $\AA$. No Band gap. It is a metal. 

   <img src="assets/image-20210611092853206.png" alt="image-20210611092853206" style="zoom:33%;" /> 

   (test)
