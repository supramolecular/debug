# Wrong band structure of BaSnO3 from OpenMX

I try to use Openmx to calculate the band structure of cubic perovskite BaSnO3. I compare the bands from Openmx and  vasp but I find they are totally different, which is shown below:   

<img src="./image-20210421170023162.png" alt="image-20210421170023162" style="zoom:50%;" /> <img src="./image-20210421170039008.png" alt="image-20210421170039008" style="zoom:50%;" /> 

I check the output file and find it has reached the convergence. I have no ideas now. Could you please help me? Input and output files can be found in the directory containing this file.

More detais:
I relax the structure and find the lattice constant a= 4.132 angstrom. For vasp, a=4.069 angstrom.
I also once tried the calculation using a=4.069 for OpenMX but get the similar results.   
